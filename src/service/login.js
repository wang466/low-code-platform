import eFlyNetWork from '@/service/_request.js';
class eFlyLogin{
    //获取验证码 返回是图片，支持get请求，直接用 img 标签解析
    
    //系统后台登录-登录
    async login(params){ return eFlyNetWork.post('/login',params) }
    //系统后台登录-登出
    async logout(params){ return eFlyNetWork.get('/logout',params) }
}
export default new eFlyLogin();