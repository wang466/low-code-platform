/**
 * 新增、编辑、删除、更新 列表区域
 * 
 * 新增、编辑、删除、更新 按钮区域
 * 
 * 新增、编辑、删除、更新 表格区域
*/
import { readJson } from './nativeFile.js';

const fs = window.preload.fs;
import { assignment } from '../base/common.js';

function getProject(){
    return eval("(" + localStorage.getItem('projectConfig') + ")").nowProject;
}

/***********************输入框相关方法***************************/
//添加输入框
async function addInput(nowItem,params){
    let nowProject = getProject();
    const result = await readJson(nowItem);
    //新增数组
    result.searchForm.push(params);
    //设置model值
    result.searchFormModel[params.model] = "";
    //写入
    await fs.writeJsonSync(`${window.preload.dirname}output/${nowProject.id}/json/${nowItem.name}.json`, result)
}

//删除输入框
async function deleteInput(nowItem,params,index){
    let nowProject = getProject();
    const result = await readJson(nowItem);
    //删除数据
    result.searchForm.splice(index,1);
    //删除对应的model值
    delete result.searchFormModel[params.model];
    //写入
    await fs.writeJsonSync(`${window.preload.dirname}output/${nowProject.id}/json/${nowItem.name}.json`, result)
}
/***********************按钮相关方法***************************/
//添加按钮
async function addTableBtn(nowItem,item){
    let nowProject = getProject();
    const result = await readJson(nowItem);
    result.btnUse.push(item);
    //写入
    await fs.writeJsonSync(`${window.preload.dirname}output/${nowProject.id}/json/${nowItem.name}.json`, result)
}

//删除按钮
async function deleteTableBtn(nowItem,index){
    let nowProject = getProject();
    const result = await readJson(nowItem);
    result.btnUse.splice(index,1);
    //写入
    await fs.writeJsonSync(`${window.preload.dirname}output/${nowProject.id}/json/${nowItem.name}.json`, result)
}
/***********************表格相关方法***************************/
//新增表格某一项
async function addTableList(nowItem,item){
    let nowProject = getProject();
    const result = await readJson(nowItem);
    result.table.push(item);
    //写入
    await fs.writeJsonSync(`${window.preload.dirname}output/${nowProject.id}/json/${nowItem.name}.json`, result)
}
//删除表格某一项
async function deleteTableList(nowItem,index){
    let nowProject = getProject();
    const result = await readJson(nowItem);
    //删除数据
    result.table.splice(index,1);
    //写入
    await fs.writeJsonSync(`${window.preload.dirname}output/${nowProject.id}/json/${nowItem.name}.json`, result)
}
//更新表格某一项
async function updateTableList(nowItem,tableConfig){
    let nowProject = getProject();
    const result = await readJson(nowItem);
    //
    assignment(tableConfig,result.tableConfig);
    //写入
    await fs.writeJsonSync(`${window.preload.dirname}output/${nowProject.id}/json/${nowItem.name}.json`, result)
}

export{
    addInput,
    deleteInput,
    addTableBtn,
    deleteTableBtn,
    addTableList,
    deleteTableList,
    updateTableList
}
