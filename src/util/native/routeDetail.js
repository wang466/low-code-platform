const fs = window.preload.fs;

function getProject(){
    return eval("(" + localStorage.getItem('projectConfig') + ")").nowProject;
}

async function createDetailFile(name){
    let nowProject = getProject();
    try{
        await fs.copySync(`${window.preload.dirname}output/template/json/blank.json`,`${window.preload.dirname}output/${nowProject.id}/json/detail/${name}.json`);
        return { code:0,message:"新建成功" }
    } catch(err) {
        return { code:0,message:"新建失败" }
    }
}

//新建路由对象
function createDetailRouteObj(params){
    var createParams = {
        "detailName":params.sideBarName,
        "name":`detail/${params.name}`,
        "path":params.path,
        "component":`() => import('@/view${params.path}.vue')`,
        "sideBarShow":false,
        "meta":{
            "title":params.sideBarName
        },
        "auth":"admin",
        "belongTo":params.belongTo
    }
    return createParams;
}

async function readRouterDetailJson(){
    let nowProject = getProject();
    const result = await fs.readFile(`${window.preload.dirname}output/${nowProject.id}/router/routerDetail.json`,'utf8');
    let useResult = eval('(' + result + ')');
    return useResult
}

async function rewriteDetailRoute(params){
    let nowProject = getProject();
    await fs.writeJsonSync(`${window.preload.dirname}output/${nowProject.id}/router/routerDetail.json`, params)
}

async function removeSyncJson(item){
    let nowProject = getProject();
    await fs.removeSync(`${window.preload.dirname}output/${nowProject.id}/json/${item.name}.json`);
}

export{
    createDetailFile,
    createDetailRouteObj,
    rewriteDetailRoute,
    readRouterDetailJson,
    removeSyncJson
}