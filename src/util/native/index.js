import * as route from './route.js';
import * as nativeFile from './nativeFile.js';
import * as output from './output.js';
import * as load from './load.js';
import * as listTemplate from './listTemplate.js';
import * as project from './project.js'; 
import * as routeDetail from './routeDetail.js'; 
var native = {
    ...route,
    ...nativeFile,
    ...output,
    ...load,
    ...listTemplate,
    ...project,
    ...routeDetail
}
export { native }