const fs = window.preload.fs;

function getProject(){
    return eval("(" + localStorage.getItem('projectConfig') + ")").nowProject;
}

async function loadPreviewComponent(name){
    let nowProject = getProject();
    return import(`${window.preload.dirname}output/${nowProject.id}/preview/${name}.vue`);
}

async function loadRouterConfig(){
    let nowProject = getProject();
    const result = await fs.readFile(`${window.preload.dirname}output/${nowProject.id}/router/routerConfig.json`,'utf8');
    let useResult = eval('(' + result + ')');
    return useResult;
}

async function loadVue(){
    let nowProject = getProject();
    const result = await fs.readFile(`${window.preload.dirname}output/${nowProject.id}/preview/aaa.vue`,'utf8');
    return result;
}

export{
    loadPreviewComponent,
    loadRouterConfig,
    loadVue
}