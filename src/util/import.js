//设置环境
if (process.env.NODE_ENV == 'development') {
    //开发环境：不使用懒加载模式
    function _import(file){
        return file;
    }
    export{
        _import
    }

} else {
    //生产环境：使用懒加载模式
    function _import(file){
        return file;
    }
    export{
        _import
    }
    
}