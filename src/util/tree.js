function treeToArray(tree) {
	let res = []
	for (const item of tree) {
		const { secMenu, ...i } = item
		if (secMenu && secMenu.length) {
			secMenu.forEach(element => {
				element.fatherPath = item.path;
			});
			res = res.concat(treeToArray(secMenu))
		}
		res.push(i)
	}
	return res
}
/**
 * 该方法用于将有父子关系的数组转换成树形结构的数组
 * 接收一个具有父子关系的数组作为参数
 * 返回一个树形结构的数组
 */
function arrayToTree(data) {
	//没有父节点的数据
	let parents = data.filter(value => value.fatherPath == 'undefined' || value.fatherPath == null)

	//有父节点的数据
	let secMenu = data.filter(value => value.fatherPath !== 'undefined' && value.fatherPath != null)

	//定义转换方法的具体实现
	let translator = (parents, secMenu) => {
		//遍历父节点数据
		parents.forEach((parent) => {
			//遍历子节点数据
			secMenu.forEach((current, index) => {
				//此时找到父节点对应的一个子节点
				if (current.fatherPath === parent.path) {
					//对子节点数据进行深复制，这里只支持部分类型的数据深复制，对深复制不了解的童靴可以先去了解下深复制
					let temp = JSON.parse(JSON.stringify(secMenu))
					//让当前子节点从temp中移除，temp作为新的子节点数据，这里是为了让递归时，子节点的遍历次数更少，如果父子关系的层级越多，越有利
					temp.splice(index, 1)
					//让当前子节点作为唯一的父节点，去递归查找其对应的子节点
					translator([current], temp)
					delete current.fatherPath;
					//把找到子节点放入父节点的secMenu属性中
					typeof parent.secMenu !== 'undefined' ? parent.secMenu.push(current) : parent.secMenu = [current]
				}
			}
			)
		}
		)
	}
	//调用转换方法
	translator(parents, secMenu)
	//返回最终的结果
	return parents
}
/*
*过滤掉sideBarShow不是true的数组
*/
const filterTreeByFunc = (tree, func = (item) => { return item.sideBarShow === true }) => {
	if (!Array.isArray(tree) || tree.length === 0) {
		return []
	}
	return tree.filter(item => {
		item.secMenu = item.secMenu && filterTreeByFunc(item.secMenu, func)
		return func(item) || (item.secMenu && item.secMenu.length)
	})
}
//找到此节点在数组中的位置
function array_getObjPosition(arr, item) {
	let index = null;
	for (let i = 0; i < arr.length; i++) {
		let arrItem = arr[i];
		if (arrItem.path == item.path) {
			index = i;
			break;
		}
	}
	return index;
}
//判断是否有兄弟节点
function array_nodeNum(arr, item) {
	let index = 0;
	for (let i = 0; i < arr.length; i++) {
		let arrItem = arr[i];
		if (arrItem.fatherPath == item.fatherPath) {
			index++;
		}
	}
	return index;
}

function arr_findItem(arr, path) {
	let result = {};
	for (let i = 0; i < arr.length; i++) {
		let arrItem = arr[i];
		if (arrItem.path == path) {
			result = arrItem;
			break;
		}
	}
	return result;
}

//获取某节点的所有子节点
function getAllChild(nodes, item, arr) {
	for (let el of nodes) {
		if (el.fatherPath === item) {
			if (typeof el.name != "undefined") {
				arr.push({ path: el.path, name: el.name });
			} else {
				arr.push({ path: el.path });
			}
			if (typeof el.secMenu != 'undefined' && el.secMenu.length > 0) {
				childNodesDeep(el.secMenu, arr);
			}
		} else if (typeof el.secMenu != 'undefined' && el.secMenu.length > 0) {
			getAllChild(el.secMenu, item, arr);
		}
	}
	return arr;
};
function childNodesDeep(nodes, arr) {
	if (nodes) {
		nodes.forEach((ele) => {
			if (typeof ele.name != "undefined") {
				arr.push({ path: ele.path, name: ele.name });
			} else {
				arr.push({ path: ele.path });
			}
			if (typeof ele.secMenu != 'undefined' && ele.secMenu.length > 0) {
				this.childNodesDeep(ele.secMenu, arr);
			}
		})
	}
}

function array_deleteAllItem(child, arr) {
	let chlildIndex = [];
	child.forEach(item => {
		console.log(item);
		chlildIndex.push(array_getObjPosition(arr, item));
	})
	chlildIndex.forEach(index => {
		arr.splice(index, 1, {});
	})
	let formatArr = [];
	arr.forEach(item => {
		if (typeof item.path != 'undefined') {
			formatArr.push(item)
		}
	})
	return formatArr;
}

// 查找父节点
function findParent(path, list = [], result = []) {
	for (let i = 0; i < list.length; i += 1) {
		const item = list[i]
		// 找到目标
		if (item.path === path) {
			// 加入到结果中
			result.push(item)
			// 因为可能在第一层就找到了结果，直接返回当前结果
			if (result.length === 1) return result
			return true
		}
		// 如果存在下级节点，则继续遍历
		if (item.secMenu) {
			// 预设本次是需要的节点并加入到最终结果result中
			result.push(item)
			const find = findParent(path, item.secMenu, result)
			// 如果不是false则表示找到了，直接return，结束递归
			if (find) {
				return result
			}
			// 到这里，意味着本次并不是需要的节点，则在result中移除
			result.pop()
		}
	}
	// 如果都走到这儿了，也就是本轮遍历children没找到，将此次标记为false
	return false
}

function onlyPageObj(arr){
	let pageObj = {};
	arr.forEach(item => {
		if(item.name){
			pageObj[item.name] = item;
		}
	});
	return pageObj;
}


export {
	treeToArray,
	arrayToTree,
	filterTreeByFunc,
	array_getObjPosition,
	array_nodeNum,
	arr_findItem,
	getAllChild,
	array_deleteAllItem,
	findParent,
	onlyPageObj
}