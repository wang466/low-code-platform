import { createRouter, createWebHashHistory } from 'vue-router'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import routes from './routes.js'
import setting from '@/config/setting.js';

// console.log('[routes]', routes)

const router = createRouter({
    history: createWebHashHistory(),
    routes
});

/**
 * 路由拦截、权限验证
 */
router.beforeEach((to, from, next) => {
    NProgress.start()
    next()
    /*
    // 验证当前路由所有的匹配中是否需要有登录验证的
    if (to.matched.some(r => r.meta.auth)) {
        // 暂定使用 cookie 里是否存有 isLoginSuc 作为 验证是否登录 的条件，请根据自身业务需要修改
        if (!(type(localStorage.getItem('isLoginSuc')) == "null")){
            next()
        } else {
            if(to.path == "/login"){
                //不加这个判断会出现死循环问题
                next()
            }else{
                // 未登陆或登陆过期：没有登录的时候跳转到登录界面
                next('/login')
            }
        }
    } else {
        // 不需要身份校验 直接通过
        next()
    }
    */
})

router.afterEach((to, from) => {
    NProgress.done()
    if(to.meta.title){
        document.title = to.meta.title + ' - ' + setting.bro_title;
    }else{
        document.title = setting.project_name;
    }
    
})




export default router
