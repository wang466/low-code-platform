import backtop from './modules/vbacktop';
import draggable from './modules/vdraggable';
import maxlength from './modules/maxlength';

const directives = {
    backtop,
    draggable,
    maxlength
};

export default {
    install(app) {
        Object.keys(directives).forEach((key) => {
            app.directive(key, directives[key]);
        });
    }
};
