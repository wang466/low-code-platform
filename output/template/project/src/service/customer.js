import service from './_request'

// 客户列表查询
export const query = (params) => service.get('/consumer/list', params)

// 查询经销商姓名 - ID
export const queryName = (params) => service.post('/consumer/selectName', params)

// 客户信息修改
export const update = (params) => service.post('/consumer/update', params)

// 客户信息导出
export const exportInfo = async (params, filename) => {
    const res = await service.download('/consumer/export', params)
    const blob = new Blob([res.data], {
        type: res.type
    })
    const url = URL.createObjectURL(blob)
    const link = document.createElement('a')
    link.href = url
    link.download = filename || `客户信息${Date.now()}`
    link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));
    URL.revokeObjectURL(url)
}

// 地址查询
export const queryAddress = (params) => service.post('/address/list', params)

export default {
    query,
    queryName,
    update,
    exportInfo,
    queryAddress
}
