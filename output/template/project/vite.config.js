import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from "path"
// PostCss：引入自定义补全
import autoprefixer from 'autoprefixer'
// 使用 vite-plugin-mock 插件
import { viteMockServe } from "vite-plugin-mock"
// 自动导入
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
//手动按需引入
//import ElementPlus from 'unplugin-element-plus/vite'
//svg插件
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons';
// native ESM
import legacy from '@vitejs/plugin-legacy'

/**
 * warnings when minifying css: [WARNING] "@charset" must be the first rule in the file
 * 该警告由于 element-plus 某些 scss 文件中有中文注释，导致 sass 编译时，插入了 `@charset`。
 * 警告导致 vite 2.6.x-2.7.x 在 rendering chunk 时卡住。
 * 升级 vite 到 2.8.x 卡住问题解决，或使用 postcss 移除无用的 `@charset`
 */
 const charsetRemoval = {
    postcssPlugin: 'internal:charset-removal',
    AtRule: {
        charset: (atRule) => {
            if (atRule.name === 'charset') {
                atRule.remove()
            }
        }
    }
}

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {
    return {
    //部署路径
    base: './',
    server: {
        host: '0.0.0.0',
        proxy: {
            '/admin': {
                target: 'http://xx.xxx.xxx.xxx:8081',
                changeOrigin: true,
                cookieDomainRewrite: 'localhost'
            }
        }
    },
    resolve: {
        alias: {
            "@": path.resolve(__dirname, "src")
        },
    },
    plugins: [
        vue(),
        AutoImport({
            resolvers: [ElementPlusResolver()],
          }),
        Components({
            resolvers: [ElementPlusResolver()],
        }),
        legacy({
            targets: ['defaults', 'not IE 11']
        }),
        viteMockServe({
            mockPath: './mock',
            supportTs: false,
            localEnabled: true,
            prodEnabled: mode == 'mock',
            //  这样可以控制关闭mock的时候不让mock打包到最终代码内
            injectCode: `
            import { setupProdMockServer } from './mockProdServer';
            setupProdMockServer();
            `,
        }),
        createSvgIconsPlugin({
            // 指定需要缓存的图标文件夹
            iconDirs: [path.resolve(process.cwd(), 'src/plugins/svg')],
            // 指定symbolId格式
            symbolId: 'icon-[dir]-[name]',
            /**
             * 自定义插入位置
             * @default: body-last
             * 可选：'body-first'
             */
            inject: 'body-last',
            /**
             * custom dom id
             * @default: __svg__icons__dom__
             */
            customDomId: '__svg__icons__dom__',
        }),
    ],
    css: {
        preprocessorOptions: {
            less: {
                javascriptEnabled: true,
                additionalData: `@import "${path.resolve(__dirname, 'src/style/common.less')}";`
            }
        },
        postcss: {
            plugins: [
                autoprefixer,
                charsetRemoval
            ]
        }
    },
    }
})
