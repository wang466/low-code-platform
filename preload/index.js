import { ipcRenderer } from "electron";
import { join } from "path";
const fs = require('fs-extra');
const swig = require('swig');
swig.setDefaults({ cache: false });
const compressing = require('compressing');
window.preload = {
    dirname:join(__dirname,'../../../'),
    fs:fs,
    swig:swig,
    compressing:compressing,
    openChildWindow: async () => await ipcRenderer.invoke('open-child-window'),
    selectOutputPath: async () => await ipcRenderer.invoke('select-output-path'),
    isPackaged:async () => await ipcRenderer.invoke('isPackaged'),
    setMainWinSizeLarge: async () => await ipcRenderer.invoke('set-mainWin-large'),
    setMainWinSizeSmall: async () => await ipcRenderer.invoke('set-mainWin-small'),
    previewDemoProject: async () => await ipcRenderer.invoke('preview-demo-project'),
};